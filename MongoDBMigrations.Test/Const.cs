﻿namespace MongoDBMigrations.Test
{
    public static class Const
    {
        public static class TestDatabase
        {
            public const string ConnectionString = "mongodb://localhost:27017";
            public const string DatabaseName = "test";
        }
    }
}