﻿using System;

namespace MongoDBMigrations
{
    /// <summary>
    /// Instantiates migrations using System.Activator.
    /// </summary>
    public class ActivatorMigrationFactory : IMigrationFactory
    {
        /// <summary>
        /// Create migration object using System.Activator.
        /// </summary>
        /// <param name="migrationType">Migration type.</param>
        /// <returns>Migration object.</returns>
        public IMigration CreateMigration(Type migrationType)
        {
            return (IMigration)Activator.CreateInstance(migrationType);
        }
    }
}
