﻿using System;

namespace MongoDBMigrations
{
    /// <summary>
    /// Instantiates migrations.
    /// </summary>
    public interface IMigrationFactory
    {
        /// <summary>
        /// Instantiate an object of a given migration type.
        /// </summary>
        /// <param name="migrationType">Migration type.</param>
        /// <returns>Migration object.</returns>
        IMigration CreateMigration(Type migrationType);
    }
}
